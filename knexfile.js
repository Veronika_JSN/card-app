module.exports = {

  development: {
    client: 'mysql2',
    connection: {
      port: '3306',
      host: 'localhost',
      database: 'default',
      user:     'default',
      password: 'secret'
    },
    migrations: {
      directory: './db/migrations'
    },
    seeds: {
      directory: './db/seeds'
    }
  }

};
