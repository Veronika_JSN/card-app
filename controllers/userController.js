const userService = require('../services/userService');

exports.login = async (req, res, next) => {

    try {
        const {nickname, password} = req.body;
        const tokens = await userService.login(nickname, password);
        res.status(200).json(tokens);
    } catch (e) {
        req.error = e;
        next()
    }
};

exports.signUp = async (req, res, next) => {

    try {
        const {nickname, password, confPass} = req.body;
        const tokens = await userService.signUp(nickname, password, confPass);
        res.status(200).json(tokens);
    } catch (e) {
        req.error = e;
        next()
    }
};

exports.refreshToken = async (req, res, next) =>{

    try {
        const {id, token} = req.userId;
        const tokens = await userService.renewalToken(id, token);
        res.status(200).json(tokens);
    } catch (e) {
        req.error = e;
        next()
    }
}

exports.create = async (req, res, next) => {

    try {
        const {nickname, password, isAdmin} = req.body;
        await userService.create(nickname, password, isAdmin);
        res.status(204).send();
    } catch (e) {
        req.error = e;
        next()
    }
};

exports.edit = async (req, res, next) => {

    try {
        const {id} = req.params;
        const {nickname, password, isAdmin} = req.body;
        await userService.edit(id, nickname, password, isAdmin);
        res.status(204).send();
    } catch (e) {
        req.error = e;
        next()
    }
}

exports.usersList = async (req, res, next) => {

    try {
        const {page, pageSize} = req.query;
        const users = await userService.usersList(page, pageSize);
        res.status(200).json(users);
    } catch (e) {
        req.error = e;
        next()
    }
}

exports.getSets = async (req, res, next) => {

    try {
        const {id: user_id} = req.user;
        const sets = await userService.getSets(user_id);
        res.status(200).json(sets);
    } catch (e) {
        req.error = e;
        next()
    }
}

exports.info = async (req, res, next) => {

    try {
        const {id: user_id} = req.user;
        const userInfo = await userService.info(user_id);
        res.status(200).json(userInfo);
    } catch (e) {
        req.error = e;
        next()
    }
}

exports.tableRating = async (req, res, next) => {

    try {
        const rating = await userService.rating();
        res.status(200).json(rating);
    } catch (e) {
        req.error = e;
        next()
    }
}