const cardService = require('../services/cardService');

exports.cardList = async (req, res, next) => {

    try {
        const {page, pageSize} = req.query;
        const cards = await cardService.cardList(page, pageSize);
        res.status(200).json(cards);
    } catch (e) {
        req.error = e;
        next()
    }
};

exports.card = async (req, res, next) => {

    try {
        const {id} = req.params;
        const card = await cardService.card(id);
        res.status(200).json(card);
    } catch (e) {
        req.error = e;
        next()
    }
};

exports.create = async (req, res, next) =>{

    try {
        await cardService.create(req.body);
        res.status(204).send();
    } catch (e) {
        req.error = e;
        next()
    }
}

exports.edit = async (req, res, next) => {

    try {
        await cardService.edit(req.body, req.params.id);
        res.status(204).send();
    } catch (e) {
        req.error = e;
        next()
    }
}

exports.cardIdList = async (req, res, next) => {

    try {
        const idsList = await cardService.cardIdList();
        res.status(200).json(idsList);
    } catch (e) {
        req.error = e;
        next()
    }
};

