const setService = require('../services/setService');

exports.create = async (req, res, next) => {

    try {
        const {award, card_id} = req.body;
        await setService.create(award, card_id);
        res.status(204).send();
    } catch (e) {
        req.error = e;
        next()
    }
};

exports.setsList = async (req, res, next) => {

    try {
        const {page, pageSize} = req.query;
        const sets = await setService.setList(page, pageSize);
        res.status(200).json(sets);
    } catch (e) {
        req.error = e;
        next()
    }
}

exports.edit = async (req, res, next) => {

    try {
        const {id} = req.params;
        const {award, card_id} = req.body;
        await setService.edit(id, award, card_id);
        res.status(204).send();
    } catch (e) {
        req.error = e;
        next()
    }
}

