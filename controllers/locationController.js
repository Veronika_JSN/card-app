const locationService = require('../services/locationService');

exports.getAll = async (req, res, next) => {

    try {
        const locations = await locationService.getAll();
        res.status(200).json(locations);
    } catch (e) {
        req.error = e;
        next()
    }
};
