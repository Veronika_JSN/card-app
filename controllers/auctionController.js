const auctionService = require('../services/auctionService');

exports.create = async (req, res, next) => {

    try {
        await auctionService.create(req.body, req.user);
        res.status(204).send();
    } catch (e) {
        req.error = e;
        next()
    }
};

exports.auctionsList = async (req, res, next) => {

    try {
        const {page, pageSize} = req.query;
        const sets = await auctionService.auctionsList(page, pageSize);
        res.status(200).json(sets);
    } catch (e) {
        req.error = e;
        next()
    }
}

exports.rate = async (req, res, next) => {

    try {
        const {id: auction_id} = req.params;
        const {id: user_id} = req.user;
        const {rate} = req.body;
        await auctionService.rate(auction_id, user_id, rate);
        res.status(204).send();
    } catch (e) {
        req.error = e;
        next()
    }
}