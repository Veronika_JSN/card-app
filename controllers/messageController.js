const messageService = require('../services/messageService');

exports.create = async (req, res, next) => {

    try {
        const {id} = req.user;
        const {text} = req.body;
        await messageService.create(id, text);
        res.status(204).send();
    } catch (e) {
        req.error = e;
        next()
    }
};

exports.messagesList = async (req, res, next) => {

    try {
        const {page, pageSize} = req.query;
        const messages = await messageService.messagesList(page, pageSize);
        res.status(200).json(messages);
    } catch (e) {
        req.error = e;
        next()
    }
}