const repositoriesAuction = require('../repositories/auction');
const repositoriesUserCard = require('../repositories/userCard');
const repositoriesBalance = require('../repositories/balance');
const repositoriesUser = require('../repositories/user');
const customError = require('../src/utils/customErorrs');
const {timeLeft} = require('../config')

exports.create = async (data, params) => {

    const realTime = new Date();
    const {is_admin, id} = params;

    const {
        initial_rate,
        min_bet_step,
        max_rate,
        card_id,
        duration,
        min_auction_extension_time
    } = data;

    const durationTime = new Date(realTime.setMinutes(realTime.getMinutes() + Number(duration)));

    if (!is_admin) {

        const card = await repositoriesUserCard.getCard(id, card_id);

        createAuction(id, initial_rate, min_bet_step, durationTime, min_auction_extension_time, max_rate, card.id);

    } else {

        const card = await repositoriesUserCard.create(card_id);

        createAuction(
            id,
            initial_rate,
            min_bet_step,
            durationTime,
            min_auction_extension_time,
            max_rate,
            card.id
        )
    }
}

async function createAuction(id, initial_rate, min_bet_step, durationTime, min_auction_extension_time, max_rate, card_id) {

    const auction = await repositoriesAuction.getByCard(card_id);

    if (auction) {
        throw new customError.UnprocessableEntity('This card is already up for auction')
    }

    await repositoriesAuction.create({
        seller_id: id,
        initial_rate,
        min_bet_step,
        duration_of_the_auction: durationTime,
        min_auction_extension_time,
        max_rate,
        card_id,
    });
}


exports.auctionsList = (page, pageSize) => {
    return repositoriesAuction.getAuctionsList(page, pageSize);
}

exports.rate = async (auction_id, user_id, userRate) => {

    const auction = await repositoriesAuction.getOne(auction_id);
    const serializedAuction = auction.serialize();

    const {
        initial_rate,
        min_bet_step,
        max_rate,
        min_auction_extension_time,
        duration_of_the_auction,
        rate
    } = serializedAuction;

    const bit = rate || initial_rate;

    if (userRate <= bit || userRate <= bit + min_bet_step || userRate >= max_rate) {
        throw new customError.UnprocessableEntity('Сheck if the rate is correct')
    }

    if (duration_of_the_auction < new Date()) {
        throw new customError.UnprocessableEntity('Bids are not accepted')
    }

    const balance = await repositoriesBalance.getBalance(user_id);

    const rates = await repositoriesAuction.getAllRateUser(user_id, auction_id);

    const total = balance - rates - userRate;

    if (total < 0) {
        throw new customError.Forbidden(`You don't have enough money`)
    }

    auction.set({
        participant_id: user_id,
        rate: userRate,
        updated_at: new Date()
    })

    const realTime = new Date();

    const endAuction = new Date(duration_of_the_auction);

    const permissible = new Date(endAuction.setMinutes(endAuction.getMinutes() - Number(timeLeft)));


    if (permissible < realTime) {

        const newDurTime = new Date(realTime.setMinutes(realTime.getMinutes() + Number(min_auction_extension_time)));

        auction.set({
            duration_of_the_auction: newDurTime
        })
    }
    auction.save();
}


exports.over = async () => {

    const auctions = await repositoriesAuction.getOverTimeAuction();

    for (const auction of auctions) {

        const {seller_id, participant_id, rate, card_id} = auction.serialize();

        const user = await repositoriesUser.isAdmin(seller_id);

        if (!rate && !!user) {

            await repositoriesUserCard.destroy(card_id);
            return;
        }

        if (rate) {
            await repositoriesAuction.over(seller_id, participant_id, rate, card_id);
        }

        auction.set({active: 0});
        await auction.save();

    }
}