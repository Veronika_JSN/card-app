const repositoriesMessage = require('../repositories/message');

exports.messagesList = (page, pageSize) => {
    return repositoriesMessage.getMessagesList(page, pageSize);
}

exports.create = (id, text) => {
    return repositoriesMessage.create({
        writer_id: id,
        text: text,
        created_at: new Date()
    })
}
