const repositoriesUser = require('../repositories/user');
const repositoriesUserCard = require('../repositories/userCard');
const repositoriesSet = require('../repositories/set');
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const {JWT_ACCESS_KEY, JWT_REFRESH_KEY} = require('../config');
const customError = require('../src/utils/customErorrs');
const repositoriesBalance = require("../repositories/balance");

const generateRefToken = (id, key, time) => {
    const payload = {
        id: id
    }
    return jwt.sign(payload, key, {expiresIn: time})
}


exports.login = async (nickname, password) => {

    const user = await repositoriesUser.getUserByField('nickname', nickname);

    if (!user) {
        throw new customError.BadRequestError(`Check your email or password`);
    }

    const match = await bcrypt.compare(password, user.get('password'));

    if (!match) {
        throw new customError.BadRequestError(`Check your email or password`);
    }

    const serializedUser = user.serialize();


    const accessToken = jwt.sign(serializedUser, JWT_ACCESS_KEY, {expiresIn: 60 * 30});
    const refreshToken = generateRefToken(serializedUser.id, JWT_REFRESH_KEY, '30d');

    user.set('refresh_token', refreshToken);
    user.save();

    return {
        accessToken,
        refreshToken
    }
}


exports.signUp = async (nickname, password, confPass) => {
    if (password !== confPass) {
        throw new customError.BadRequestError('password and confirm password are different');
    }

    const candidate = await repositoriesUser.getUserByField('nickname', nickname);

    if (candidate) {
        throw new customError.BadRequestError(`It's nickname already selected`);
    }

    const hash = await bcrypt.hash(password, 10);
    const createdUser = await repositoriesUser.createUser({
        nickname: nickname,
        password: hash,
    })

    const serializedUser = createdUser.serialize();

    const accessToken = jwt.sign(serializedUser, JWT_ACCESS_KEY, {expiresIn: 60 * 30});
    const refreshToken = generateRefToken(serializedUser.id, JWT_REFRESH_KEY, '30d');

    createdUser.set('refresh_token', refreshToken);
    createdUser.save();

    return {
        accessToken,
        refreshToken
    };
}


exports.renewalToken = async (id, token) => {
    const candidate = await repositoriesUser.getUserByField('id', id);
    const serializedCandidate = candidate.serialize();

    if (!serializedCandidate.refreshToken === token) {
        throw new customError.BadToken(`you should re-login`);
    }

    const accessToken = jwt.sign(serializedCandidate, JWT_ACCESS_KEY, {expiresIn: 60 * 30});
    const refreshToken = generateRefToken(serializedCandidate.id, JWT_REFRESH_KEY, '30d');

    candidate.set('refresh_token', refreshToken);
    candidate.save();

    return {
        accessToken,
        refreshToken
    }
}


exports.create = async (nickname, password, isAdmin) => {

    const candidate = await repositoriesUser.getUserByField('nickname', nickname);

    if (candidate) {
        throw new customError.BadRequestError(`It's nickname already selected`);
    }

    const hash = await bcrypt.hash(password, 10);

    await repositoriesUser.createUser({
        nickname: nickname,
        password: hash,
        is_admin: isAdmin
    })
}


exports.edit = async (id, nickname, password, isAdmin) => {

    const user = await repositoriesUser.getUserByField('id', id);

    if (!user) {
        throw new customError.BadRequestError(`User not found`);
    }

    if (password) {
        const hash = await bcrypt.hash(password, 10);

        user.set({
            nickname: nickname,
            password: hash,
            is_admin: isAdmin
        });
    } else {
        user.set({
            nickname: nickname,
            is_admin: isAdmin
        });
    }

    user.save();
}


exports.usersList = (page, pageSize) => {
    return repositoriesUser.getUsersList(page, pageSize);
}


async function getUserSets(user_id) {
    let userSets = [];

    const userCards = await repositoriesUserCard.getUserCards(user_id);
    const serializeUserCards = userCards.serialize();
    const cards = serializeUserCards.map(card => card.card_id);

    const sets = await repositoriesSet.getAll();
    const serializeSets = sets.serialize();

    for (let i = 0; i < serializeSets.length; i++) {
        const setId = serializeSets[i].id;
        const setCards = serializeSets[i].cards.map(item => item.id);

        for (let j = 0; j < setCards.length; j++) {

            if (!cards.includes(setCards[j])) {
                break
            }
            if (j === setCards.length - 1) {
                userSets.push(setId)
            }
        }
    }
    return {userSets, sets}
}


function getRatings(commonAndUserSets) {

    let sum = 0;

    commonAndUserSets.sets.forEach(set => {
        const id = set.get('id');
        if (commonAndUserSets.userSets.includes(id)) {
            sum += set.get('award');
        }
    })
    return sum
}


exports.getSets = async (user_id) => {
    const commonAndUserSets = await getUserSets(user_id);
    return commonAndUserSets.userSets
}


exports.info = async (user_id) => {
    const commonAndUserSets = await getUserSets(user_id);
    const userRating = getRatings(commonAndUserSets);
    const balance = await repositoriesBalance.getBalance(user_id);
    const user = await repositoriesUser.getUserByField('id', user_id);

    return {
        userRating,
        balance,
        user
    }
}


exports.rating = async () => {
    const users = await repositoriesUser.getAllUsers();
    const serializeUsers = users.serialize();

    const rating = Promise.all(await serializeUsers.map(async user => {

        const commonAndUserSets = await getUserSets(user.id)

        const sum = getRatings(commonAndUserSets);

        return ({nickname: user.nickname, rating: sum});
    }))

    return rating

}
