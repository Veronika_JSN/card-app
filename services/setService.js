const repositoriesSet = require('../repositories/set');

exports.create = async (award, card_id) => {
    const set = await repositoriesSet.create({
        award
    })
    await set.cards().attach(card_id);
}

exports.setList = (page, pageSize) => {
    return repositoriesSet.getSetList(page, pageSize);
}

exports.edit = async (id, award, cards) => {

    const set = await repositoriesSet.getSet(id);
    set.set({
        award,
        updated_at: new Date()
    })
    set.save();

    await set.cards().detach();
    await set.cards().attach(cards);

}