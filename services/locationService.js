const repositoriesLocation = require('../repositories/location');

exports.getAll = () => {
    return repositoriesLocation.getAll();
}
