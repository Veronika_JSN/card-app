const repositoriesCard = require('../repositories/card');

exports.cardList = (page, pageSize) => {
    return repositoriesCard.getCardList(page, pageSize);
}

exports.card = async (id) => {
    const card = await repositoriesCard.getCard(id);
    return card.serialize();
}

exports.create = async (data) => {
    const {name, status, species, type, gender, image, location_id, episode_id} = data;
    const card = await repositoriesCard.create({
        name,
        status,
        species,
        type,
        gender,
        image,
        location_id
    })

    await card.episodes().attach(episode_id);
}

exports.edit = async (data, id) => {

    const {name, status, species, type, gender, image, location_id, episode_id} = data;

    const card = await repositoriesCard.getCard(id);
    card.set({
        name,
        status,
        species,
        type,
        gender,
        image,
        location_id,
        updated_at: new Date()
    })
    card.save();

    await card.episodes().detach();
    await card.episodes().attach(episode_id);

}

exports.cardIdList = () => {
    return repositoriesCard.cardIdList();
}
