const URL = 'https://rickandmortyapi.com/api';
const REACT_APP_WEB_URL='http://localhost:3000';
const PORT = 5000;
const JWT_ACCESS_KEY = 'gv2hmuy9FZCayXV2VV12';
const JWT_REFRESH_KEY = 'MyBaudXeg2bH7jhKuf10';
const timeLeft = 5;

module.exports = {
    URL,
    REACT_APP_WEB_URL,
    PORT,
    JWT_ACCESS_KEY,
    JWT_REFRESH_KEY,
    timeLeft
};
