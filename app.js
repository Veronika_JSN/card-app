const express = require('express');
const {PORT} = require('./config');
const cors = require('cors');
const userRoutes = require('./routes/user');
const cardRoutes = require('./routes/card');
const messageRoutes = require('./routes/message');
const setRoutes = require('./routes/set');
const auctionRoutes = require('./routes/auction');
const locationRoutes = require('./routes/location');
const {socketOn} = require('./socket/socketInvoker');
const checkErrors = require('./middleware/checkErrors');
require('./cron/cronInvoker');


const app = express();
const server = require('http').createServer(app);

socketOn(server);

app.use(express.json());
app.use(express.urlencoded({
        extended: true,
    })
);

app.use(cors({
    origin: "*",
}));

app.use("/users", userRoutes);
app.use("/cards", cardRoutes);
app.use("/message", messageRoutes);
app.use("/sets", setRoutes);
app.use("/auctions", auctionRoutes);
app.use("/locations", locationRoutes);


app.use(checkErrors);


server.listen(PORT, () => console.log(`Server has been started on port ${PORT}...`));

module.exports = app;