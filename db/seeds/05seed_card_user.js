exports.seed = function(knex) {

  return knex('user_card').del()
    .then(function () {

      return knex('user_card').insert([
          {user_id: 2, card_id: 5},
          {user_id: 2, card_id: 6},
          {user_id: 4, card_id: 1},
          {user_id: 4, card_id: 2},
          {user_id: 4, card_id: 5},
      ]);
    });
};
