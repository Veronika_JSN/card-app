exports.seed = function(knex) {

    return knex('balance').del()
        .then(function () {

            return knex('balance').insert([
                {user_id: '1', action: 200},
                {user_id: '4', action: 200},
                {user_id: '4', action: -50},
                {user_id: '4', action: 100},
                {user_id: '4', action: -70}
            ]);
        });
};
