exports.up = function(knex) {
    return knex.schema.table('sets', table => {
        table.dateTime('updated_at');
        table.dateTime('created_at');
    })
};

exports.down = function(knex) {
    return knex.schema.table('sets', table => {
        table.dropColumn('updated_at');
        table.dropColumn('created_at');
    })
};