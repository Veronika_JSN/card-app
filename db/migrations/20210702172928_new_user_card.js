exports.up = function(knex) {
    return new Promise((res, rej) => {
        knex.schema.table('user_card', (tableBuilder) => {
            tableBuilder.dropForeign('card_id');
        }).then(() => {
            return knex.schema.table('user_card', (tableBuilder) => {
                tableBuilder.foreign('card_id').references('id').inTable('cards').onDelete('CASCADE');
            })
        }).then(res, rej)
    })
};

exports.down = function(knex) {
    return new Promise((res, rej) => {
        knex.schema.table('user_card', (tableBuilder) => {
            tableBuilder.dropForeign('card_id');
        }).then(() => {
            return knex.schema.table('user_card', (tableBuilder) => {
                tableBuilder.foreign('card_id').references('id').inTable('cards');
            })
        }).then(res, rej)
    })
};