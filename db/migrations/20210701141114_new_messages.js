exports.up = function(knex) {
    return knex.schema.table('messages', table => {
        table.dateTime('created_at');
    })
};

exports.down = function(knex) {
    return knex.schema.table('messages', table => {
        table.dropColumn('created_at');
    })
};