exports.up = function(knex) {
    return new Promise((res, rej) => {
        knex.schema.table('auctions', (tableBuilder) => {
            tableBuilder.dropColumn('duration_of _the_auction');
        }).then(() => {
            return knex.schema.table('auctions', (tableBuilder) => {
                tableBuilder.dateTime('duration_of_the_auction');
                tableBuilder.integer('participant_id').unsigned();
                tableBuilder.foreign('participant_id').references('id').inTable('users').onDelete('cascade');
                tableBuilder.integer('rate');
                tableBuilder.dateTime('created_at');
                tableBuilder.dateTime('updated_at');
            })
        }).then(()=>{
                return knex.schema.table('auctions', (tableBuilder) => {
                    tableBuilder.dateTime('min_auction_extension_time').alter();
            })
        }).then(res, rej)
    })
};

exports.down = function(knex) {
    return new Promise((res, rej) => {
        knex.schema.table('auctions', (tableBuilder) => {
            tableBuilder.time('duration_of _the_auction');
        }).then(() => {
            return knex.schema.table('auctions', (tableBuilder) => {
                tableBuilder.dropColumn('duration_of_the_auction');
                tableBuilder.dropForeign('participant_id');
                tableBuilder.dropColumn('participant_id');
                tableBuilder.dropColumn('rate');
                tableBuilder.dropColumn('created_at');
                tableBuilder.dropColumn('updated_at');
            })
        }).then(()=>{
            return knex.schema.table('auctions', (tableBuilder) => {
                tableBuilder.time('min_auction_extension_time').alter();
            })
        }).then(res, rej)
    })
};