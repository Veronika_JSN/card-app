exports.up = function(knex) {
    return new Promise((res, rej) => {
        knex.schema.table('auctions', (tableBuilder) => {
            tableBuilder.dropForeign('card_id');
        }).then(() => {
            return knex.schema.table('auctions', (tableBuilder) => {
                tableBuilder.foreign('card_id').references('id').inTable('user_card').onDelete('CASCADE');
            })
        }).then(res, rej)
    })
};

exports.down = function(knex) {
    return new Promise((res, rej) => {
        knex.schema.table('auctions', (tableBuilder) => {
            tableBuilder.dropForeign('card_id');
        }).then(() => {
            return knex.schema.table('auctions', (tableBuilder) => {
                tableBuilder.foreign('card_id').references('id').inTable('user_card');
            })
        }).then(res, rej)
    })
};