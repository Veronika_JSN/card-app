exports.up = function(knex) {
    return knex.schema.table('auctions', table => {
        table.boolean('active').defaultTo(true);
    })
};

exports.down = function(knex) {
    return knex.schema.table('auctions', table => {
        table.dropColumn('active');
    })
};