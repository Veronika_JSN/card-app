
exports.up = function(knex) {
  return knex.schema.table('auctions', (tableBuilder) => {
      tableBuilder.integer('min_auction_extension_time').alter();
  })
};

exports.down = function(knex) {
    return knex.schema.table('auctions', (tableBuilder) => {
        tableBuilder.dateTime('min_auction_extension_time').alter();
    })
};


