exports.up = function(knex) {
    return new Promise((res, rej) => {
        knex.schema.table('locations', (tableBuilder) => {
            tableBuilder.dropColumn('update_at');
        }).then(() => {
            return knex.schema.table('locations', (tableBuilder) => {
                tableBuilder.dateTime('updated_at');
            })
        }).then(res, rej)
    })
};

exports.down = function(knex) {
    return new Promise((res, rej) => {
        knex.schema.table('locations', (tableBuilder) => {
            tableBuilder.dropColumn('updated_at');
        }).then(() => {
            return knex.schema.table('locations', (tableBuilder) => {
                tableBuilder.dateTime('update_at');
            })
        }).then(res, rej)
    })
};