const cron = require('node-cron');
const {runAuctionEndJob} = require('./auctionEndJob')

cron.schedule('* * * * *', () => {
    runAuctionEndJob()
    },{
        scheduled: true
});


