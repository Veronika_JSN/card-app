const auctionService = require('../services/auctionService');

const runAuctionEndJob =  async () => {
    await auctionService.over()
};

module.exports = {
    runAuctionEndJob
};