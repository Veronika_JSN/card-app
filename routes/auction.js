const express = require("express");
const router = express.Router();
const auctionController = require('../controllers/auctionController');
const {verifyToken} = require('../middleware/checkAuth');

router.post("/", verifyToken, auctionController.create);
router.get("/", verifyToken, auctionController.auctionsList);
router.patch("/:id", verifyToken, auctionController.rate);

module.exports = router;