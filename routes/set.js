const express = require("express");
const router = express.Router();
const setController = require('../controllers/setController');
const {verifyToken} = require('../middleware/checkAuth');
const {verifyAdmin} = require('../middleware/checkAdmin');

router.get("/", setController.setsList);
router.post("/", verifyToken, verifyAdmin, setController.create);
router.put("/:id", verifyToken, verifyAdmin, setController.edit);

module.exports = router;