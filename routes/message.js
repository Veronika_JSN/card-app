const express = require("express");
const router = express.Router();
const messageController = require('../controllers/messageController');
const {verifyToken} = require('../middleware/checkAuth');

router.get("/", verifyToken, messageController.messagesList);
router.post("/", verifyToken, messageController.create);

module.exports = router;