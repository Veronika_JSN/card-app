const db_bookshelf = require('../src/utils/db')
const Set = require('./set');

const User = db_bookshelf.model('User', {
    tableName: 'users',
    sets() {
        return this.belongsToMany(Set, 'set_user', 'user_id', 'set_id')
    },
    serialize(){
        return {
            id: this.attributes.id,
            is_admin: this.attributes.is_admin,
            nickname: this.attributes.nickname
        }
    },
    requireFetch: false,
})

module.exports = User;