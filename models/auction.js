const db_bookshelf = require('../src/utils/db');
const User = require('./user');
const UserCard = require('./userCard');

const Auction = db_bookshelf.model('Auction', {
    tableName: 'auctions',

    seller() {
        return this.belongsTo(User, 'seller_id')
    },

    participant() {
        return this.belongsTo(User, 'participant_id')
    },

    user_card() {
        return this.belongsTo(UserCard, 'card_id')
    },
    hasTimestamps: true,
    requireFetch: false,
})

module.exports = Auction;