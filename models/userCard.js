const db_bookshelf = require('../src/utils/db');
const Card = require('./card');
const User = require('./user')

const UserCard = db_bookshelf.model('User_card', {
    tableName: 'user_card',
    cards() {
        return this.belongsTo(Card, 'card_id')
    },
    users() {
        return this.belongsTo(User, 'user_id')
    },
    requireFetch: false,
})

module.exports = UserCard;