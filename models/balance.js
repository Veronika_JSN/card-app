const db_bookshelf = require('../src/utils/db');
const User = require('./card')

const Balance = db_bookshelf.model('Balance', {
    tableName: 'balance',
    user() {
        return this.belongsTo(User, 'user_id')
    },
})

module.exports = Balance;