const db_bookshelf = require('../src/utils/db');
const User = require('./user')

const Message = db_bookshelf.model('Message', {
    tableName: 'messages',
    users() {
        return this.belongsTo(User, 'writer_id')
    }
})

module.exports = Message;