const db_bookshelf = require('../src/utils/db');
const Card = require('./card');
const User = require('./user')

const Set = db_bookshelf.model('Set', {
    tableName: 'sets',
    cards() {
        return this.belongsToMany(Card, 'card_set', 'set_id', 'card_id')
    },
    users() {
        return this.belongsToMany(User, 'set_user', 'set_id', 'user_id')
    },
    hasTimestamps: true,
    requireFetch: false,
})

module.exports = Set;