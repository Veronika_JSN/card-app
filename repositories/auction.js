const Auction = require('../models/auction');
const db_bookshelf = require('../src/utils/db');
const customError = require('../src/utils/customErorrs');


exports.create = (obj) => {
    return Auction.forge(obj).save();
};


exports.getAuctionsList = async (page, pageSize) => {

    const auctionList = await Auction.fetchPage({

        pageSize: pageSize,
        page: page,
        withRelated: [{
            seller: (qb) => qb.select('users.id', 'nickname'),
            participant: (qb) => qb.select('users.id', 'nickname'),
            user_card: (qb) => qb.select('user_card.id', 'user_card.card_id'),
            'user_card.cards': (qb) => qb.select('id', 'name'),
        }]
    });

    return {
        auctionList,
        quantity: auctionList.pagination.rowCount
    }
};


exports.getOne = async (id) => {

    const auction = await Auction.where({id: id}).fetch();

    if (!auction) {
        throw new customError.BadRequestError(`No such auction exists`);
    }
    return auction;
};


exports.getAllRateUser = async (user_id, auction_id) => {

    const rawResult = await Auction.query((qb) => {
        qb.select(db_bookshelf.knex.raw('sum(rate) AS sum')).where('participant_id', user_id).andWhereNot('id', auction_id);
    }).fetch();

    const rates = rawResult.serialize();

    return rates.sum;

};


exports.getOverTimeAuction = () => {

    return Auction.query(function (qb) {
        qb.where('active', true).andWhereNot('duration_of_the_auction', '>=', new Date());
    }).fetchAll()

}


exports.getByCard = (id) => {

    return Auction.query((qb) => {
        qb.select('id').where('card_id', id).andWhereNot('active', 0);
    }).fetch();

};


exports.over = (seller_id, participant_id, rate, card_id) => {

    db_bookshelf.transaction((trx) => {

        return db_bookshelf.knex('balance')
            .transacting(trx)
            .insert({user_id: seller_id, action: rate})

            .then(function () {
                return db_bookshelf.knex('balance')
                    .transacting(trx)
                    .insert({user_id: participant_id, action: -rate})
            })

            .then(function () {

                return db_bookshelf.knex('user_card')
                    .transacting(trx)
                    .update({user_id: participant_id})
                    .where('id', card_id)
            })

            .catch(error => console.log(error))

            .then(trx.commit)
            .catch((e) => {
                console.log(e)
                trx.rollback();
            })
    })
}