const Balance = require('../models/balance');
const db_bookshelf = require('../src/utils/db');


exports.getBalance = async (id) => {

    const rawResult = await Balance.query((qb) => {
        qb.select(db_bookshelf.knex.raw('sum(action) AS sum')).where('user_id', id);
    }).fetch();

    const balance = rawResult.serialize();

    return balance.sum;

};

exports.put = (obj) => {
    // used knex because table has not id field
    return db_bookshelf.knex('balance').insert(obj);
};