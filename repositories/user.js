const User = require('../models/user');

exports.getUserByField = (field, value) =>{
    return User.where({[field]: value}).fetch();
};

exports.createUser = (obj) =>{
    return User.forge(obj).save();
};

exports.getUsersList = async (page, pageSize) => {

    const usersList =  await User.fetchPage({
        pageSize: pageSize,
        page: page
    })

    return {
        usersList,
        quantity: usersList.pagination.rowCount
    }

};

exports.isAdmin = async (id) => {

    const isAdmin = await User.forge({id}).fetch();

    return isAdmin.get('is_admin');
};

exports.getAllUsers = async () => {

    return User.query((qb) => {
        qb.select('id', 'nickname');
    }).fetchAll();

};