const Set = require('../models/set');
const customError = require('../src/utils/customErorrs');

exports.create = (obj) => {
    return Set.forge(obj).save();
};

exports.getSetList = async (page, pageSize) => {

    const setList = await Set.fetchPage({

        pageSize: pageSize,
        page: page,
        withRelated: [{
            cards: (qb) => qb.select('name')
        }]
    });

    return {
        setList,
        quantity: setList.pagination.rowCount
    }
};

exports.getSet = async (id) => {

    const set = await Set.where({id: id}).fetch({
        withRelated: [{
            cards: (qb) => qb.select('id', 'name'),
        }]
    });

    if (!set) {
        throw new customError.BadRequestError(`No such set exists`);
    }
    return set;
};

exports.getAll = () => {
    return Set.fetchAll({
        withRelated: [{
            cards: (qb) => qb.select('id'),
        }]
    })
};