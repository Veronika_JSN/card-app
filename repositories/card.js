const Card = require('../models/card');
const customError = require('../src/utils/customErorrs');

exports.getCardList = async (page, pageSize) => {

    const cardsList = await Card.fetchPage({

        pageSize: pageSize,
        page: page,
        withRelated: [{
            episodes: (qb) => qb.select('name'),
            locations: (qb) => qb.select('id', 'name')
        }]
    });

    return {
        cardsList,
        quantity: cardsList.pagination.rowCount
    }
};

exports.getCard = async (id) => {

    const card = await Card.where({id: id}).fetch({
        withRelated: [{
            episodes: (qb) => qb.select('episodes.id', 'name'),
            locations: (qb) => qb.select('id', 'name')
        }]
    });

    if (!card) {
        throw new customError.BadRequestError(`No such card exists`);
    }
    return card;
};

exports.create = (obj) => {
    return Card.forge(obj).save();
};

exports.cardIdList = async () => {
    return Card.fetchAll({columns: ['id']});
};
