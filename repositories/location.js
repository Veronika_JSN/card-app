const Location = require('../models/locations');

exports.getAll = async () => {
    return Location.query((qb) => {
        qb.select('id', 'name')
    }).fetchAll();
};


