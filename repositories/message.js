const Message = require('../models/message');
const User = require('../models/user');

exports.getMessagesList = async (page, pageSize) => {
    const messagesList =  await Message.fetchPage({
        pageSize: pageSize,
        page: page,
        withRelated: [{
            users: (qb) => qb.select('id', 'nickname')
        }]
    });

    return {
        messagesList,
        quantity: messagesList.pagination.rowCount
    }
};

exports.create = async (obj) => {

    const msg = await Message.forge(obj).save();
    const serializedMsg = msg.serialize();

    const userName = await User.query((qb) => {
        qb.select('nickname').where('id', serializedMsg.writer_id);
    }).fetch();

    serializedMsg.userName = userName.get('nickname');
    return serializedMsg
};
