const UserCard = require('../models/userCard');

exports.getCard = (user_id, card_id) =>{
    return UserCard.where({user_id: user_id, card_id: card_id}).fetch();
};

exports.create = (card_id) =>{
    return UserCard.forge({user_id: null, card_id: card_id}).save();
};

exports.edit = (card_user_id, user_id) =>{
    return UserCard.forge({id: card_user_id}).set({user_id: user_id}).save();
};

exports.edit = (card_user_id, user_id) =>{
    return UserCard.forge({id: card_user_id}).set({user_id: user_id}).save();
};

exports.destroy = (card_user_id) =>{
    return UserCard.forge().where({id: card_user_id}).destroy();
};

exports.getUserCards = (user_id) =>{
    return UserCard.query((qb)=>{qb.where({user_id: user_id}).select('card_id')}).fetchAll();
};
