const messageService = require('../services/messageService');
const {generateId} = require('../src/utils/generateId');
const {REACT_APP_WEB_URL} = require('../config');

const addMessage = ({writerId, text})  => {
       return messageService.create(writerId, text);
}


const socketOn = (server) => {
       const io = require('socket.io')(server, {
              cors: {
                     origin: REACT_APP_WEB_URL,
                     credentials: true
              }
       });

       io.on('connection', socket => {
              const name = socket.handshake.auth.user;

              socket.emit('message', {id: generateId(), text: 'Welcome to Chat'});

              socket.broadcast.emit('message', {id: generateId(), text: `${name} has joined the chat`});

              socket.on('disconnect', () => {
                     io.emit('message', {id: generateId(), text: `${name} has left the chat`})
              })

              socket.on('chatMessage', async (socket) => {
                     const msg = await addMessage(socket)
                     io.emit('message', msg)
              })
       })
}

module.exports = {
       socketOn
};