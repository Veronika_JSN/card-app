const {over} = require('./services/auctionService')

const run = async () => {
    const task = process.argv[2];
    switch (task) {
        case 'auction_process':
            await over();
            break;
        default:
            console.log(`task: '${task}' not found`);
    }
    process.exit(1);
}

run();